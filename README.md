# CERIcompiler

A simple compiler.
From : Pascal-like imperative LL(k) langage
To : 64 bit 80x86 assembly langage (AT&T)

Basé sur [le travail de Dr. Pierre Jourlin](https://framagit.org/jourlin/cericompiler)

### Ceci est la branche principale qui doit être évaluée

**Download the repository :**

> git clone git@framagit.org:Seb_rocca/CERICOMPILER.git

**Build the compiler and test it :**

*tester la compilation:*
> make test 

*tester la fonction display:*
> make test_display

*tester la boucle for:*
> make test_for

*tester la boucle fordownto:*
> make test_fordownto 

*tester la boucle while do:*
> make test_while

*tester la fonction switch case:*
> make test_case

*tester la fonction if else:*
> make test_if

*tester la boucle do while:*
> make test_dowhile

**Have a look at the output :**

> gedit ./resultat_assembleur/<nom_du_fichier_de_test>.s

**Debug the executable :**

> ddd ./executable/<nom_du_fichier_de_test>

**Commit the new version :**

> git commit -a -m "What's new..."

**Send to your framagit :**

> git push -u origin master

**Get from your framagit :**

> git pull -u origin master

**This version Can handle :**

**NOUVEAU:** 

// DoWhileStatement := "DO" Statement "WHILE" Expression

// ForStatement := "FOR" AssignementStatement "To" Expression "DO" Statement | "FOR" AssignementStatement "DOWNTO" Expression "DO" Statement  *(mise à jour)* 

// CaseStatement := "CASE" Expression "of" CaseListElement { CaseListElement } "ELSE" Statement "END"

// Statement := IfStatement | ForStatement | WhileStatement | CaseStatement | DoWhileStatement | DisplayStatement | BlockStatement | AssignementStatement *(mise à jour)* 

**AUTRE IMPLEMENTATIONS DEJA PRESENTES AUPARAVANT:**

// Program := [DeclarationPart] StatementPart

// DeclarationPart := "[" Identifier {"," Identifier} "]"

// StatementPart := Statement {";" Statement} "."

// Statement := AssignementStatement

// AssignementStatement := Identifier ":=" Expression

// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]

// WhileStatement := "WHILE" Expression DO Statement

// ForStatement := "FOR" AssignementStatement "To" Expression "DO" Statement

// BlockStatement := "BEGIN" Statement { ";" Statement } "END"

// Expression := SimpleExpression [RelationalOperator SimpleExpression]

// SimpleExpression := Term {AdditiveOperator Term}

// Term := Factor {MultiplicativeOperator Factor}

// Factor := Number | Letter | "(" Expression ")"| "!" Factor

// Number := Digit{Digit}

// Identifier := Letter {(Letter|Digit)}

// AdditiveOperator := "+" | "-" | "||"

// MultiplicativeOperator := "*" | "/" | "%" | "&&"

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  

// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"

// Letter := "a"|...|"z"






