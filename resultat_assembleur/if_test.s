			# This code was produced by the CERI Compiler
	.data
	.align 8
FormatString1:
	.string  "%llu\n" 
a:	.quad 0
b:	.quad 0
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
IF1:
	push $1
	push $1
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	je Vrai2	# If equal
	push $0		# False
	jmp Suite2
Vrai2:	push $0xFFFFFFFFFFFFFFFF		# True
Suite2:
	pop %rax
	movq %rax, %rbx
	cmpq %rax, %rbx
	je ELSE1
THEN1:
	push $3
	push $3
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop a
	jmp END1
ELSE1:
	push $5
	push $3
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop b
END1:
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
