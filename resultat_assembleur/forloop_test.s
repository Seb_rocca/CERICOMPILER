			# This code was produced by the CERI Compiler
	.data
	.align 8
FormatString1:
	.string  "%llu\n" 
a:	.quad 0
b:	.quad 0
i:	.quad 0
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
ForInit1:
	push $0
	pop i
ForTest1:
	push i
	push $3
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	jb Vrai2	# If below
	push $0		# False
	jmp Suite2
Vrai2:	push $0xFFFFFFFFFFFFFFFF		# True
Suite2:
	pop %rax 
	cmpq $0, %rax
	je ForFin1
	push b
	push $1
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop b
	movq (i), %rax
	addq $1, %rax
	movq %rax,i
	jmp ForTest1
ForFin1:
	push i
	pop %rdx
	movq $FormatString1, %rsi
	movl $1, %edi;
	movl $0, %eax;
	call __printf_chk@PLT
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
