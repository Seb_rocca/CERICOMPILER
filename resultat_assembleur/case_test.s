			# This code was produced by the CERI Compiler
	.data
	.align 8
FormatString1:
	.string  "%llu\n" 
i:	.quad 0
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	push $3
	pop i
CASE_BEGIN1:
	push i
	pop %rcx
CASE2:
	push $3
	pop %rax
	cmpq %rax, %rcx
	jne CASE3
	push $6
	pop %rdx
	movq $FormatString1, %rsi
	movl $1, %edi;
	movl $0, %eax;
	call __printf_chk@PLT
	jmp CASEENDING1
CASE3:
	push $4
	pop %rax
	cmpq %rax, %rcx
	jne CASE4
	push $8
	pop %rdx
	movq $FormatString1, %rsi
	movl $1, %edi;
	movl $0, %eax;
	call __printf_chk@PLT
	jmp CASEENDING1
CASE4:
	push $0
	pop %rdx
	movq $FormatString1, %rsi
	movl $1, %edi;
	movl $0, %eax;
	call __printf_chk@PLT
CASEENDING1:
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
