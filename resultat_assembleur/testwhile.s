			# This code was produced by the CERI Compiler
	.data
	.align 8
FormatString1:
	.string  "%llu\n" 
a:	.quad 0
b:	.quad 0
i:	.quad 0
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	push $0
	pop b
BEGIN1:
While1:
	push b
	push $1
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	jbe Vrai3	# If below or equal
	push $0		# False
	jmp Suite3
Vrai3:	push $0xFFFFFFFFFFFFFFFF		# True
Suite3:
	pop %rax	# Get the result of expression
	cmpq $0, %rax
	je EndWhile1	# if FALSE, jump out of the loop1
	push b
	push $1
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop b
	jmp While1
EndWhile1:
	push b
	pop %rdx
	movq $FormatString1, %rsi
	movl $1, %edi;
	movl $0, %eax;
	call __printf_chk@PLT
ENDING1:
	push b
	pop %rdx
	movq $FormatString1, %rsi
	movl $1, %edi;
	movl $0, %eax;
	call __printf_chk@PLT
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
