all:	test
clean:
		rm *.o *.s
		rm tokensiser.cpp
tokeniser.cpp:	tokeniser.l
		flex++ -d -otokeniser.cpp tokeniser.l
tokeniser.o:	tokeniser.cpp
		g++ -c tokeniser.cpp
compilateur:	compilateur.cpp tokeniser.o
		g++ -ggdb -fPIC -fno-pie -no-pie -o  compilateur compilateur.cpp tokeniser.o

test: compilateur ./tests/if_test.p
		./compilateur <./tests/if_test.p > ./resultat_assembleur/if_test.s
		gcc -ggdb -fno-pie -no-pie ./resultat_assembleur/if_test.s -o ./executable/if_test

test_display:		compilateur ./tests/display_test.p
		./compilateur <./tests/display_test.p > ./resultat_assembleur/display_test.s
		gcc -ggdb -fno-pie -no-pie ./resultat_assembleur/display_test.s -o ./executable/display_test

test_for:		compilateur ./tests/forloop_test.p
		./compilateur <./tests/forloop_test.p > ./resultat_assembleur/forloop_test.s
		gcc -ggdb -fno-pie -no-pie ./resultat_assembleur/forloop_test.s -o ./executable/forloop_test

test_fordownto:		compilateur ./tests/fordownto_test.p
		./compilateur <./tests/fordownto_test.p > ./resultat_assembleur/fordownto_test.s
		gcc -ggdb -fno-pie -no-pie ./resultat_assembleur/fordownto_test.s -o ./executable/fordownto_test

test_while:		compilateur ./tests/testwhile.p
		./compilateur <./tests/testwhile.p > ./resultat_assembleur/testwhile.s
		gcc -ggdb -fno-pie -no-pie ./resultat_assembleur/testwhile.s -o ./executable/testwhile

test_case: compilateur ./tests/case_test.p
		./compilateur <./tests/case_test.p > ./resultat_assembleur/case_test.s
		gcc -ggdb -fno-pie -no-pie ./resultat_assembleur/case_test.s -o ./executable/case_test

test_if: compilateur ./tests/if_test.p
		./compilateur <./tests/if_test.p > ./resultat_assembleur/if_test.s
		gcc -ggdb -fno-pie -no-pie ./resultat_assembleur/if_test.s -o ./executable/if_test

test_dowhile: compilateur ./tests/dowhile_test.p
		./compilateur <./tests/dowhile_test.p > ./resultat_assembleur/dowhile_test.s
		gcc -ggdb -fno-pie -no-pie ./resultat_assembleur/dowhile_test.s -o ./executable/dowhile_test






