//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"

#include <map>
#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum OPTYPE {INT, BOOL};


TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

map<string, enum OPTYPE> declaredvariables; 
set<string> DeclaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
	
		
OPTYPE Identifier(void){
	OPTYPE var;
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	var = INT;
	return var; 
	
}

OPTYPE Number(void){
	OPTYPE var; 
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();
	var = INT;
	return var;
}

OPTYPE Expression(void);			// Called by Term() and calls Term()

OPTYPE Factor(void){
	OPTYPE type;
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		type = Expression();
		 
		
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else 
		if (current==NUMBER)
			type = Number();
	     	else
				if(current==ID)
					type = Identifier();
				else
					Error("'(' ou chiffre ou lettre attendue");
	return type; 
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;

}

// Term := Factor {MultiplicativeOperator Factor}
OPTYPE Term(void){
	OPMUL mulop;
	OPTYPE factor_val = Factor();
	OPTYPE factor_val2;
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		factor_val2 = Factor();
		
		if(factor_val != factor_val2)
		{
		Error("Type attendu: " + factor_val );
		}
		
		
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(mulop){
			case AND:
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# MUL"<<endl;	// store result
				break;
			case DIV:
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
				cout << "\tpush %rax\t# DIV"<<endl;		// store result
				break;
			case MOD:
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return factor_val;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
OPTYPE SimpleExpression(void){
	OPADD adop;
	OPTYPE term_val = Term();
	OPTYPE term_val2; 
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		term_val2 = Term();
		if(term_val != term_val2)
		{
			Error("Type attendu: " + term_val);
		}
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(adop){
			case OR:
				cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				break;			
			case ADD:
				cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
				break;			
			case SUB:	
				cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
				break;
			default:
				Error("opérateur additif inconnu");
		}
		cout << "\tpush %rax"<<endl;			// store result
	}
	return term_val; 

}

// DeclarationPart := "[" Ident {"," Ident} "]" --> "VAR" VarDeclaration {";" VarDeclaration} "."
void DeclarationPart(void){
	if(current!=RBRACKET)
		Error("caractère '[' attendu");
	cout << "\t.data"<< endl;
	cout << "\t.align 8"<< endl;
	cout << "FormatString1:" << endl;
	cout << "\t.string  \"%llu\\n\" " << endl;   

	

	current=(TOKEN) lexer->yylex();
	if(current!=ID)
		Error("Un identificater était attendu");
	cout << lexer->YYText() << ":\t.quad 0"<<endl;
	DeclaredVariables.insert(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		cout << lexer->YYText() << ":\t.quad 0"<<endl;
		DeclaredVariables.insert(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(current!=LBRACKET)
		Error("caractère ']' attendu");
	current=(TOKEN) lexer->yylex();
}

void VarDeclaration(void);

/*
// DeclarationPart := "[" Ident {"," Ident} "]" --> "VAR" VarDeclaration {";" VarDeclaration} "."
void VarDeclarationPart(void){
	if(current != VARDEC || strcmp (lexer->YYText(), "VAR") != 0)
	{
		Error("VAR attendu pour une déclaration de variable");
	}
	current=(TOKEN) lexer->yylex();
	VarDeclaration();

	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();


}

//VarDeclaration := Ident {"," Ident} ":" Type
void VarDeclaration(void)
{

} */

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}



// Expression := SimpleExpression [RelationalOperator SimpleExpression]
OPTYPE Expression(void){
	OPREL oprel;
	 
	OPTYPE simple_exp1 = SimpleExpression();
	OPTYPE simple_exp2; 
	if(current==RELOP){
		oprel=RelationalOperator();
		simple_exp2 = SimpleExpression();
		if(simple_exp1 != simple_exp2)
		{
			Error("Type attendu: " + simple_exp1); 
		}
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
	return BOOL;
	}
	return simple_exp1;
}

// AssignementStatement := Identifier ":=" Expression
string AssignementStatement(void){
	string variable;
	OPTYPE t1;
	OPTYPE t2;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	
	t1 = declaredvariables[variable];
	
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	
	t2 = Expression();
	if(t1 != t2)
	{
		Error("Type attendu: " + t1);
	}
	cout << "\tpop "<<variable<<endl;
	
	return variable; 
}




void IfStatement(void);
void ForStatement(void);
void CaseStatement(void); 
void WhileStatement(void); 
void DoWhileStatement(void);
void DisplayStatement(void); 
void BlockStatement (void);


// Statement := AssignementStatement
void Statement(void){
	
	
	if(strcmp(lexer->YYText(),"IF") == 0)
	{
		IfStatement();
	}
	else if (strcmp(lexer->YYText(), "FOR") == 0)
	{
		
		ForStatement();
	}	
	else if (strcmp(lexer->YYText(), "WHILE") == 0)
	{
		
		WhileStatement(); 
	}

	else if (strcmp(lexer->YYText(), "CASE") == 0)
	{
		
		CaseStatement();
	}

	else if(strcmp(lexer->YYText(), "DO") == 0)
	{
		DoWhileStatement(); 
	}

	else if (strcmp(lexer->YYText(), "DISPLAY")  == 0) 
	{
		DisplayStatement();
	}

	else if (strcmp(lexer->YYText(), "BEGIN") == 0)
	{
		BlockStatement();
	}
	
	
	else
	{
		AssignementStatement();
	}
	
	
	
}



//IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
void IfStatement(void){
	//IF viens d'être lu
	unsigned long tagnum = ++TagNumber;
	//int tagNum = ++TagNumber; 
	cout << "IF" << tagnum << ":" << endl;
	current=(TOKEN) lexer->yylex(); 
	
	OPTYPE bool_or_not = Expression(); 
	
	if(bool_or_not != BOOL) 
	{
		Error("Type BOOLEEN attendu"); 
	}
	
	cout << "\tpop %rax" << endl;
	cout << "\tmovq %rax, %rbx" << endl;
	cout << "\tcmpq %rax, %rbx" << endl; 
	cout << "\tje ELSE" << tagnum << endl;
	
	
	if(current != STATEMENTKEYWORD || strcmp(lexer->YYText(), "THEN") != 0)
	{
		Error("THEN ATTENDU");
	}
	cout << "THEN" << tagnum << ":" << endl;
	current=(TOKEN) lexer->yylex();
	Statement();
	cout << "\tjmp END" << tagnum << endl; 
	current=(TOKEN) lexer->yylex(); 
	cout << "ELSE" << tagnum << ":" << endl; 
	
	if(current == STATEMENTKEYWORD && strcmp(lexer->YYText(), "ELSE") == 0)
	{
		current = (TOKEN) lexer->yylex();
		Statement(); 
	}
	
	cout << "END"<< tagnum << ":" << endl;   
	
}

//ForStatement := "FOR" AssignementStatement "TO" Expression "DO" Statement | "FOR" AssignementStatement "DOWNTO" Expression "DO" Statement
void ForStatement(void)
{
	unsigned long tagnum = ++TagNumber;
	cout << "ForInit" << tagnum << ":" << endl; 
	
	current=(TOKEN) lexer->yylex(); //"FOR" -> AssignementStatement  
	
	string var = AssignementStatement(); 
	
	//current=(TOKEN) lexer->yylex();   //AssignementStatement -> "To" 
	
	if(strcmp (lexer->YYText(), "TO") == 0)
	{
		cout << "ForTest" << tagnum << ":" << endl; 
		 
		current=(TOKEN) lexer->yylex(); // To -> Expression 
		//string var = AssignementStatement();
	
		Expression();
	
		cout << "\tpop %rax " << endl;
		cout << "\tcmpq $0, %rax" << endl;
		cout << "\tje ForFin" << tagnum<<  endl;
	
		//current=(TOKEN) lexer->yylex(); // Expression -> "DO" 
	
		if(current != STATEMENTKEYWORD || strcmp (lexer->YYText(), "DO") != 0)
		{
			Error("DO ATTENDU");
		}
	
		current=(TOKEN) lexer->yylex();  // "DO" -> Statement 
	
		Statement();
		cout << "\tmovq (" << var << "), %rax" << endl;
		cout << "\taddq $1, %rax" << endl; 
		cout << "\tmovq %rax," << var << endl;
		cout << "\tjmp ForTest" << tagnum <<  endl; 
	
		cout << "ForFin" << tagnum << ":" << endl;
	}

	else if (strcmp (lexer->YYText(), "DOWNTO") == 0)
	{
		cout << "ForTest" << tagnum << ":" << endl; 
		
		current=(TOKEN) lexer->yylex(); // To -> Expression 
		//string var = AssignementStatement();
	
		Expression();
	
		cout << "\tpop %rax " << endl;
		cout << "\tcmpq $0, %rax" << endl;
		cout << "\tje ForFin" << tagnum<<  endl;
	
		//current=(TOKEN) lexer->yylex(); // Expression -> "DO" 
	
		if(current != STATEMENTKEYWORD || strcmp (lexer->YYText(), "DO") != 0)
		{
			Error("DO ATTENDU");
		}
	
		current=(TOKEN) lexer->yylex();  // "DO" -> Statement 

		Statement();
		cout << "\tmovq (" << var << "), %rax" << endl;
		cout << "\tsubq $1, %rax" << endl; 
		cout << "\tmovq %rax," << var << endl;
		cout << "\tjmp ForTest" << tagnum <<  endl; 
	
		cout << "ForFin" << tagnum << ":" << endl;
	}
	
	else
	{
		Error("TO attendu");
	}
	 
} 

void WhileStatement(void) 
{
	unsigned long long tag=TagNumber++;
	cout<<"While"<<tag<<":"<<endl;
	
	current=(TOKEN) lexer->yylex();
	
	if(Expression()!=BOOL)
		Error("expression booléene attendue");
	cout<<"\tpop %rax\t# Get the result of expression"<<endl;
	cout<<"\tcmpq $0, %rax"<<endl;
	cout<<"\tje EndWhile"<<tag<<"\t# if FALSE, jump out of the loop"<<tag<<endl;
	
	if(current!=STATEMENTKEYWORD||strcmp(lexer->YYText(), "DO")!=0)
		Error("DO attendu");
	current=(TOKEN) lexer->yylex();
	Statement();
	cout<<"\tjmp While"<<tag<<endl;
	cout<<"EndWhile"<<tag<<":"<<endl;
}

void DoWhileStatement(void)
{
	unsigned long tagnum = ++TagNumber;
	if(current!=STATEMENTKEYWORD||strcmp(lexer->YYText(), "DO")!=0)
		Error("DO attendu");
	cout<<"do_while"<< tagnum << ":" << endl;
	current=(TOKEN) lexer->yylex();
	
	Statement();
	
	if(current!=STATEMENTKEYWORD||strcmp(lexer->YYText(), "WHILE")!=0)
		Error("WHILE attendu");
	cout << "while" << tagnum << ":" << endl;
	current=(TOKEN) lexer->yylex();
	 
	if(Expression()!=BOOL)
		Error("expression booléene attendue");
	cout<<"\tpop %rax\t# Get the result of expression"<<endl;
	cout<<"\tcmpq $0, %rax"<<endl;
	cout<<"\tje EndWhile"<<tagnum<<"\t# if FALSE, jump out of the loop" << tagnum << endl; 
	cout << "\tjmp do_while" << tagnum << endl; 

	cout << "EndWhile" << tagnum << ":" << endl; 
	

}



int CaseListElement(int tagg);

//CaseStatement ::= "CASE" <expression> "of" <case list element> {; <case list element> } "END" 
void CaseStatement(void)
{
	int tag_end; 
	//CASE viens d'être lu
	unsigned long tagnum = ++TagNumber;
	
	cout << "CASE_BEGIN" << tagnum << ":" << endl;
	
	current=(TOKEN) lexer->yylex(); 
	
	Expression();

	
	cout << "\tpop %rcx" << endl;    
	
	
	if(current != STATEMENTKEYWORD && strcmp(lexer->YYText(), "of") != 0)
	{
		Error("Expression 'of' attendu");
	}
	
	
	current=(TOKEN) lexer->yylex(); 
	while(current != STATEMENTKEYWORD && strcmp(lexer->YYText(), "ELSE") != 0)
	{
	tag_end = CaseListElement(tagnum);
	}
	
	if(current != STATEMENTKEYWORD && strcmp(lexer->YYText(), "ELSE") != 0)
	{
		Error("ELSE attendu dans une structure CASE");
	}
	
	cout << "CASE" << tag_end+1 << ":" << endl; 
	current=(TOKEN) lexer->yylex();
	
	if(current != CASETEST)
	{
		
		Error(" ':' attendu lors d'un CASE");
	}
	
	current =(TOKEN) lexer->yylex();
	
	Statement();

	if(current != STATEMENTKEYWORD && strcmp(lexer->YYText(), "END") != 0)
	{
		Error("END attendu dans une structure CASE");
	}

	cout << "CASEENDING" << tagnum << ":" << endl; 

	current=(TOKEN) lexer->yylex();


}

//CaseListElement() ::= CaseLabelList : Statement | <empty>
int CaseListElement(int tagg)
{
	unsigned long tagnum = ++TagNumber;
	//cout << TagNumber << endl; 
	 
	cout << "CASE" << tagnum << ":" << endl;

	Expression(); 
	
	cout << "\tpop %rax" << endl; 
	cout << "\tcmpq %rax, %rcx" << endl; 
	cout << "\tjne " << "CASE" << tagnum+1 << endl; 
 
	if(current != CASETEST)
	{
		
		Error(" ':' attendu lors d'un CASE");
	}
	
	current=(TOKEN) lexer->yylex(); 

	if(strcmp(lexer->YYText(), "") != 0)
	{
		Statement();
	}

	cout << "\tjmp CASEENDING" << tagg << endl; 
	
	return TagNumber; 

	
}



 

//BlockStatement := "BEGIN" Statement { ";" Statement } "END"
void BlockStatement (void)
{
	unsigned long tagnum = ++TagNumber;
	if(current != STATEMENTKEYWORD && strcmp (lexer->YYText(), "BEGIN") != 0)
	{
		Error("BEGIN attendu");
		
	}

	cout << "BEGIN" << tagnum << ":" << endl; 
	current =(TOKEN) lexer->yylex();
	
	while(strcmp (lexer->YYText(), "END") != 0)
	{
	Statement();
	if(current != SEMICOLON)
	{
		
		Error("caractère ; attendu a la fin d'une ligne de BlockStatement"); 
	}
	current =(TOKEN) lexer->yylex();
	}
	
	current =(TOKEN) lexer->yylex();
	
	cout << "ENDING" << tagnum << ":" << endl; 
	
	
}

void DisplayStatement(void)
{
	if(current != STATEMENTKEYWORD && strcmp (lexer->YYText(), "DISPLAY") != 0)
	{
		Error("ERREUR DISPLAY");		
	}
	else
	{
		current =(TOKEN) lexer->yylex();
		Expression();
		cout << "\tpop %rdx" << endl; 
		cout << "\tmovq $FormatString1, %rsi" << endl; 
		cout << "\tmovl $1, %edi;" << endl; 
		cout << "\tmovl $0, %eax;" << endl; 
		cout << "\tcall __printf_chk@PLT" << endl;
		
	}
}


// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==RBRACKET)
		DeclarationPart();
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
		
			





